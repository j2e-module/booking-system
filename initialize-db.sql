create table
    user_profile (
        id bigserial not null bio TEXT,
        email varchar(255) unique,
        first_name varchar(255),
        last_name varchar(255),
        phone varchar(255),
        updated_at timestamp(6),
        primary key (id)
    );

create table
    users (
        id bigserial not null,
        created_at timestamp(6),
        email varchar(255),
        locked boolean default false,
        password varchar(255),
        role varchar(255) check (role in ('ROLE_USER', 'ROLE_ADMIN')),
        updated_at timestamp(6),
        profile_id bigint constraint FKo47sw84ajr24vfq4ccoha1256 references user_profile (id) on delete cascade,
        primary key (id)
    );

create table
    booking (
        id bigserial not null,
        check_in timestamp(6),
        check_out timestamp(6),
        created_at timestamp(6),
        is_cancelled boolean default false,
        is_confirmed boolean default false,
        is_paid boolean default false,
        note TEXT,
        updated_at timestamp(6),
        room_id bigint constraint FKq83pan5xy2a6rn0qsl9bckqai references room,
        user_id bigint constraint FK7udbel7q86k041591kj6lfmvw references users,
        primary key (id)
    );

create table
    equipment (
        id bigserial not null,
        description varchar(255),
        name varchar(255),
        primary key (id)
    );

create table
    room (
        id bigserial not null,
        capacity integer,
        created_at timestamp(6),
        description TEXT,
        name varchar(255),
        price float (53),
        updated_at timestamp(6),
        primary key (id)
    );

create table
    room_equipment (
        room_id bigint not null constraint FKqi32u6nxv9y6w769n87o958fj references room,
        equipment_id bigint not null constraint FKrmdm2ihfq0rjieivqjmnty6hq references equipment,
    );