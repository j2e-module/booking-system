# Hotel Booking System with Spring Framework

This repository contains the source code for a hotel room booking api built using the Spring Framework. 


## Key Technologies

* Spring Boot: Simplifies configuration and deployment for a self-contained application.
* Spring MVC: Implements the MVC model to manage user interactions through controllers.
* Spring Data JPA: Provides interaction with the database using JPA.
* Spring Security: Integrates for platform security with user authentication and access control.

## Database and Storage

This project uses PostgreSQL as relational database and stores images inside a bucket 


## Functionalities

* **Room Search & Booking:** Users can search for available rooms based on criteria like dates and number of guests. They can view details and book rooms.
* **Reservation Management:** Users can manage their reservation history and view details. Administrators can manage all reservations.
* **User Authentication & Management:** Users can create accounts, log in, and manage personal information. Administrators can manage user accounts and roles.
* **Room Management (for Administrators):** Administrators can add, modify, and remove rooms, along with managing room availability.