package com.study.bookingsys.services;

import com.study.bookingsys.models.dtos.AuthRequestDTO;
import com.study.bookingsys.models.dtos.AuthResponseDTO;
import com.study.bookingsys.models.dtos.RegisterDTO;

public interface IAuthService {

    public AuthResponseDTO authenticate(AuthRequestDTO request);

    public void register(RegisterDTO request);
}
