package com.study.bookingsys.services;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.ImageDTO;
import com.study.bookingsys.models.dtos.ProfileDTO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.multipart.MultipartFile;

public interface IProfileService {
    void updateProfile(ProfileDTO profileDTO, HttpServletRequest request) throws HttpException;

    ProfileDTO getProfile(HttpServletRequest request, Long id) throws HttpException;

    ImageDTO updateProfileImage(MultipartFile image, HttpServletRequest request) throws HttpException;
}
