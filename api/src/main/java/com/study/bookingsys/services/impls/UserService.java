package com.study.bookingsys.services.impls;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.enums.ERole;
import com.study.bookingsys.mappers.UserMapper;
import com.study.bookingsys.models.dtos.UserDTO;
import com.study.bookingsys.models.entities.User;
import com.study.bookingsys.respositories.UserRepository;
import com.study.bookingsys.services.IUserService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    User getUserById(Long id) throws HttpException {
        return userRepository.findById(id)
                .orElseThrow(() -> new HttpException("User not found", HttpStatus.NOT_FOUND));
    }

    private void checkIfNewEmailIsTaken(String email) throws HttpException {
        if (userRepository.findByEmail(email).isPresent()) {
            throw new HttpException("Email is already taken", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userMapper.entitiesToUserDTOs(userRepository.findAll());
    }

    @Override
    public UserDTO getUser(Long id) throws HttpException {
        User user = getUserById(id);
        return userMapper.entityToUserDTO(user);
    }

    @Override
    public void updateUser(Long id, UserDTO userDTO) throws HttpException {
        User user = getUserById(id);
        if(!user.getEmail().equals(userDTO.getEmail())) {
            checkIfNewEmailIsTaken(userDTO.getEmail());
        }
        userMapper.userDTOToEntity(userDTO, user);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id, HttpServletRequest request) throws HttpException {
        User user = getUserById(id);
        User currentUser = (User) request.getAttribute("user");
        if(currentUser.getId().equals(id)){
            throw new HttpException("You can't delete yourself", HttpStatus.BAD_REQUEST);
        }
        userRepository.deleteById(user.getId());
    }
}
