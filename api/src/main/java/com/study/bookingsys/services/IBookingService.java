package com.study.bookingsys.services;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.BookingDTO;
import com.study.bookingsys.models.dtos.BookingRequestDTO;
import jakarta.servlet.http.HttpServletRequest;

import java.util.List;

public interface IBookingService {

    BookingDTO bookRoom(Long id, BookingRequestDTO bookingDTO, HttpServletRequest request) throws HttpException;

    List<BookingDTO> getAllPersonalBookings(HttpServletRequest request, int page, int size);

    List<BookingDTO> getAllBookings(HttpServletRequest request, int page, int size);

    BookingDTO updateBooking(HttpServletRequest request, Long bookingId, BookingDTO bookingDTO) throws HttpException;

    void deleteBooking(HttpServletRequest request, Long bookingId) throws HttpException;
}
