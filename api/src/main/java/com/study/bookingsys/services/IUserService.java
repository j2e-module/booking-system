package com.study.bookingsys.services;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.UserDTO;
import jakarta.servlet.http.HttpServletRequest;

import java.util.List;

public interface IUserService {

    List<UserDTO> getAllUsers();

    UserDTO getUser(Long id) throws HttpException;

    void updateUser(Long id, UserDTO userDTO) throws HttpException;

    void deleteUser(Long id, HttpServletRequest request) throws HttpException;
}
