package com.study.bookingsys.services;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.BookingDTO;
import com.study.bookingsys.models.dtos.BookingRequestDTO;
import com.study.bookingsys.models.dtos.RoomDTO;
import com.study.bookingsys.models.dtos.SearchDTO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IRoomService {

    RoomDTO addRoom(RoomDTO roomDTO, List<MultipartFile> images) throws HttpException ;

    RoomDTO updateRoom(Long id, RoomDTO roomDTO, List<MultipartFile> images) throws HttpException;

    void deleteRoom(Long id) throws HttpException;

    RoomDTO getRoom(Long id) throws HttpException;

    List<RoomDTO> getAllRooms(SearchDTO searchDTO, int page, int size);
}
