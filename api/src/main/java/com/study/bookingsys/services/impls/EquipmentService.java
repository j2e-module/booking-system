package com.study.bookingsys.services.impls;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.mappers.EquipmentMapper;
import com.study.bookingsys.models.dtos.EquipmentDTO;
import com.study.bookingsys.models.entities.Equipment;
import com.study.bookingsys.models.entities.Room;
import com.study.bookingsys.respositories.EquipmentsRepository;
import com.study.bookingsys.services.IEquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipmentService implements IEquipmentService {

    @Autowired
    private EquipmentsRepository equipmentsRepository;

    @Autowired
    private EquipmentMapper equipmentMapper;

    public Equipment checkAndGetById(Long id) throws HttpException {
        return equipmentsRepository.findById(id).orElseThrow(() -> new HttpException("Equipment not found", HttpStatus.NOT_FOUND) {});
    }

    @Override
    public List<EquipmentDTO> getAllEquipments() {
        return equipmentMapper.equipmentsToEquipmentDTOs(equipmentsRepository.findAll());
    }

    @Override
    public EquipmentDTO addEquipment(EquipmentDTO equipmentDTO) {
        Equipment equipment = new Equipment();
        equipmentDTO.setId(null);
        equipmentMapper.dtoToEquipmentEntity(equipmentDTO, equipment);
        return equipmentMapper.entityToEquipmentDTO(equipmentsRepository.save(equipment));
    }

    @Override
    public EquipmentDTO updateEquipment(Long id, EquipmentDTO equipmentDTO) throws HttpException {
        Equipment equipment = checkAndGetById(id);
        equipmentDTO.setId(equipment.getId());
        equipmentMapper.dtoToEquipmentEntity(equipmentDTO, equipment);
        return equipmentMapper.entityToEquipmentDTO(equipmentsRepository.save(equipment));
    }

    @Override
    public void deleteEquipment(Long id) throws HttpException {
        Equipment equipment = checkAndGetById(id);
        for (Room room : equipment.getRoomList()) {
            room.getEquipmentList().remove(equipment);
        }
        equipmentsRepository.deleteById(id);
    }
}
