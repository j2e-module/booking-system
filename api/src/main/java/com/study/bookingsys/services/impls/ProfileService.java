package com.study.bookingsys.services.impls;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.enums.ERole;
import com.study.bookingsys.mappers.ImageMapper;
import com.study.bookingsys.mappers.ProfileMapper;
import com.study.bookingsys.models.dtos.ImageDTO;
import com.study.bookingsys.models.dtos.ProfileDTO;
import com.study.bookingsys.models.entities.Image;
import com.study.bookingsys.models.entities.Profile;
import com.study.bookingsys.models.entities.User;
import com.study.bookingsys.respositories.ProfileRepository;
import com.study.bookingsys.services.IProfileService;
import com.study.bookingsys.utils.ImageUtils;
import com.study.bookingsys.utils.UserUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ProfileService implements IProfileService {

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private ProfileMapper profileMapper;

    @Autowired
    private BucketImageService bucketImageService;

    @Autowired
    private ImageMapper imageMapper;

    public Profile getProfileById(Long id, HttpServletRequest request) throws HttpException {
        if(request.isUserInRole(ERole.ROLE_ADMIN.name())){
            return profileRepository.findByUserId(id).orElseThrow(() -> new HttpException("Profile not found", HttpStatus.NOT_FOUND) {});
        }
        return profileRepository.findSafelyById(id).orElseThrow(() -> new HttpException("Profile not found", HttpStatus.NOT_FOUND) {});
    }

    @Override
    public void updateProfile(ProfileDTO profileDTO, HttpServletRequest request) throws HttpException {
        User currentUser = UserUtils.getUserFromRequest(request);
        Profile profile = profileRepository.findById(currentUser.getId())
                .orElseThrow(() -> new HttpException("Profile not found", HttpStatus.NOT_FOUND) {});
        Profile newProfile = profileMapper.profileDTOToEntity(profileDTO);
        newProfile.setId(profile.getId());
        newProfile.setUser(profile.getUser());
        profileRepository.save(newProfile);
    }

    @Override
    public ProfileDTO getProfile(HttpServletRequest request, Long id) throws HttpException {
        Profile profile = getProfileById(id, request);
        return profileMapper.entityToProfileDTO(profile);
    }

    @Override
    public ImageDTO updateProfileImage(MultipartFile image, HttpServletRequest request) throws HttpException {
        User currentUser = UserUtils.getUserFromRequest(request);
        Profile profile = profileRepository.findByUserId(currentUser.getId())
                .orElseThrow(() -> new HttpException("Profile not found", HttpStatus.NOT_FOUND) {});
        if(ImageUtils.isImageNotValid(image)) {
            throw new HttpException("Invalid image", HttpStatus.BAD_REQUEST);
        }
        if(profile.getImage() != null) {
            bucketImageService.deleteImage(profile.getImage().getUrl());
        }
        String url = bucketImageService.uploadImage(image);
        Image imageEntity = Image.builder().name(image.getOriginalFilename()).url(url).type(image.getContentType()).build();
        profile.setImage(imageEntity);
        profileRepository.save(profile);
        return imageMapper.entityToImageDTO(imageEntity);
    }
}
