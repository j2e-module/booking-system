package com.study.bookingsys.services.impls;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.mappers.BookingMapper;
import com.study.bookingsys.mappers.RoomMapper;
import com.study.bookingsys.models.dtos.EquipmentDTO;
import com.study.bookingsys.models.dtos.RoomDTO;
import com.study.bookingsys.models.dtos.SearchDTO;
import com.study.bookingsys.models.entities.Equipment;
import com.study.bookingsys.models.entities.Image;
import com.study.bookingsys.models.entities.Room;
import com.study.bookingsys.respositories.BookingRepository;
import com.study.bookingsys.respositories.RoomRepository;
import com.study.bookingsys.services.IRoomService;
import com.study.bookingsys.specifications.SpecificationBuilder;
import com.study.bookingsys.utils.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

import static com.study.bookingsys.utils.SearchUtils.getBuilderWithSearchCriteria;

@Service
public class RoomService implements IRoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private BookingMapper bookingMapper;

    @Autowired
    private EquipmentService equipmentService;

    @Autowired
    private BucketImageService bucketImageService;

    public Room checkAndGetRoomById(Long id) throws HttpException {
        return roomRepository.findById(id).orElseThrow(
                () -> new HttpException("Room not found", HttpStatus.NOT_FOUND)
        );
    }

    private List<Equipment> checkAndGetAllEquipments(RoomDTO roomDTO) throws HttpException {
        List<Equipment> equipmentList = new ArrayList<>();
        for (EquipmentDTO equipmentDTO : roomDTO.getEquipmentList()) {
            equipmentList.add(equipmentService.checkAndGetById(equipmentDTO.getId()));
        }
        return equipmentList;
    }

    private RoomDTO saveRoom(Room room, RoomDTO roomDTO, List<MultipartFile> images) throws HttpException {
        if(ImageUtils.isImagesNotValid(images)){
            throw new HttpException("Invalid image type", HttpStatus.BAD_REQUEST);
        }
        if(!images.isEmpty() && room.getImageList() == null){
            room.setImageList(new ArrayList<>());
        }
        for (MultipartFile image : images) {
            String url = bucketImageService.uploadImage(image);
            Image imageEntity = Image.builder().name(image.getOriginalFilename())
                    .url(url).type(image.getContentType()).build();
            room.getImageList().add(imageEntity);
        }
        roomMapper.roomDTOToEntity(roomDTO, room);
        room.getEquipmentList().clear();
        room.setEquipmentList(checkAndGetAllEquipments(roomDTO));
        roomRepository.save(room);
        return roomMapper.entityToRoomDTO(room);
    }

    @Override
    public RoomDTO addRoom(RoomDTO roomDTO, List<MultipartFile> images) throws HttpException {
        Room newRoom = new Room();
        return saveRoom(newRoom, roomDTO, images);
    }

    @Override
    public RoomDTO updateRoom(Long id, RoomDTO roomDTO, List<MultipartFile> images) throws HttpException {
        Room updatedRoom = checkAndGetRoomById(id);
        if(!updatedRoom.getImageList().isEmpty()){
            for (Image image : updatedRoom.getImageList()) {
                bucketImageService.deleteImage(image.getUrl());
            }
            updatedRoom.getImageList().clear();
        }
        return saveRoom(updatedRoom, roomDTO, images);
    }

    @Override
    public void deleteRoom(Long id) throws HttpException {
        Room room  = checkAndGetRoomById(id);
        room.getEquipmentList().clear();
        roomRepository.delete(room);
    }

    @Override
    public RoomDTO getRoom(Long id) throws HttpException {
        return roomMapper.entityToRoomDTO(checkAndGetRoomById(id));
    }

    @Override
    public List<RoomDTO> getAllRooms(SearchDTO searchDTO, int page, int size) {
        SpecificationBuilder specificationBuilder = getBuilderWithSearchCriteria(searchDTO);
        PageRequest pageRequest = PageRequest.of(page, size);
        List<Room> rooms = roomRepository.findAll(specificationBuilder.buildRooms(), pageRequest).getContent();
        return roomMapper.roomEntitiesToDTOs(rooms);
    }
}