package com.study.bookingsys.services;

import org.springframework.web.multipart.MultipartFile;

public interface IBucketImage {

    String uploadImage(MultipartFile image);

    void deleteImage(String name);
}
