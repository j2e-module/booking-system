package com.study.bookingsys.services;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.EquipmentDTO;

import java.util.List;

public interface IEquipmentService {

    List<EquipmentDTO> getAllEquipments();

    EquipmentDTO addEquipment(EquipmentDTO equipmentDTO);

    EquipmentDTO updateEquipment(Long id ,EquipmentDTO equipmentDTO) throws HttpException;

    void deleteEquipment(Long id) throws HttpException;
}
