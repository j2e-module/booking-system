package com.study.bookingsys.services;

import com.study.bookingsys.configs.exceptions.HttpException;
import jakarta.servlet.http.HttpServletRequest;

public interface IAuthoritiesManagerService {

    void assertOwnerAccess(HttpServletRequest request, Long ownerId) throws HttpException;
}
