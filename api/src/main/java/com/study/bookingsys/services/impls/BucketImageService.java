package com.study.bookingsys.services.impls;

import com.study.bookingsys.configs.MinioConfig;
import com.study.bookingsys.services.IBucketImage;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;
import java.util.UUID;

@Service
public class BucketImageService implements IBucketImage {

    @Autowired
    private MinioClient minioClient;

    @Autowired
    private MinioConfig minioConfig;

    @Value("${minio.bucket-name}")
    private String bucketName;

    @Value("${minio.region}")
    private String region;

    @Value("${minio.url}")
    private String url;

    @Override
    public String uploadImage(MultipartFile file) {
        try {
            UUID uuid = UUID.nameUUIDFromBytes(file.getBytes());
            String objectName = String.format("%s.%s", uuid,
                    Objects.requireNonNull(file.getContentType()).split("/")[1]);
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(bucketName)
                            .object(objectName)
                            .region(region)
                            .stream(file.getInputStream(), file.getSize(), -1)
                            .contentType(file.getContentType())
                            .build());
            return String.format("%s/%s/%s", url, bucketName, objectName);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void deleteImage(String name) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(name).region(region).build());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
