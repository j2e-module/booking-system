package com.study.bookingsys.services.impls;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.mappers.BookingMapper;
import com.study.bookingsys.models.dtos.BookingDTO;
import com.study.bookingsys.models.dtos.BookingRequestDTO;
import com.study.bookingsys.models.entities.Booking;
import com.study.bookingsys.models.entities.Room;
import com.study.bookingsys.models.entities.User;
import com.study.bookingsys.respositories.BookingRepository;
import com.study.bookingsys.services.IBookingService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.study.bookingsys.utils.UserUtils.getUserFromRequest;

@Service
public class BookingService implements IBookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private RoomService roomService;

    @Autowired
    private BookingMapper bookingMapper;

    @Override
    public BookingDTO bookRoom(Long id, BookingRequestDTO bookingRequestDTO, HttpServletRequest request) throws HttpException {
        if( bookingRequestDTO.getCheckIn().isAfter(bookingRequestDTO.getCheckOut()) ||
            bookingRequestDTO.getCheckIn().isEqual(bookingRequestDTO.getCheckOut()) ||
            bookingRequestDTO.getCheckIn().isBefore(LocalDateTime.now())
        ) {
            throw new HttpException("Check date parameters", HttpStatus.BAD_REQUEST);
        }
        Room room = roomService.checkAndGetRoomById(id);
        if( bookingRepository.isThereBookedRoom(room.getId(), bookingRequestDTO.getCheckIn(), bookingRequestDTO.getCheckOut()) ) {
            throw new HttpException("Room is already booked", HttpStatus.BAD_REQUEST);
        }
        User user = getUserFromRequest(request);
        Booking booking = Booking.builder()
                .note(bookingRequestDTO.getNote())
                .checkIn(bookingRequestDTO.getCheckIn())
                .checkOut(bookingRequestDTO.getCheckOut()).build();
        booking.setRoom(room);
        booking.setUser(user);
        BookingDTO bookingDTO = new BookingDTO();
        bookingMapper.entityToBookingDTO(bookingRepository.save(booking), bookingDTO);
        return bookingDTO;
    }

    @Override
    public List<BookingDTO> getAllPersonalBookings(HttpServletRequest request, int page, int size) {
        User user = getUserFromRequest(request);
        PageRequest pageRequest = PageRequest.of(page, size);
        List<Booking> bookings = bookingRepository.findAllByUserId(user.getId(), pageRequest).getContent();
        return bookingMapper.entitiesToBookingDTOs(bookings);
    }

    @Override
    public List<BookingDTO> getAllBookings(HttpServletRequest request, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        return bookingMapper.entitiesToBookingDTOs(bookingRepository.findAll(pageRequest).getContent());
    }

    @Override
    public BookingDTO updateBooking(HttpServletRequest request, Long bookingId, BookingDTO bookingDTO) throws HttpException {
        Booking booking = bookingRepository.findById(bookingId)
                .orElseThrow(() -> new HttpException("Booking not found", HttpStatus.NOT_FOUND));
        LocalDateTime checkIn = bookingDTO.getCheckIn() != null ? bookingDTO.getCheckIn() : booking.getCheckIn();
        LocalDateTime checkOut = bookingDTO.getCheckOut() != null ? bookingDTO.getCheckOut() : booking.getCheckOut();
        if(bookingRepository.isThereBookedRoomExceptCurrent(booking.getRoom().getId(), checkIn, checkOut, bookingId) ) {
            throw new HttpException("Room is already booked", HttpStatus.BAD_REQUEST);
        }
        bookingMapper.bookingDTOToEntity(bookingDTO, booking);
        return bookingMapper.entityToBookingDTO(bookingRepository.save(booking));
    }

    @Override
    public void deleteBooking(HttpServletRequest request, Long bookingId) throws HttpException {
        bookingRepository.findById(bookingId)
                .orElseThrow(() -> new HttpException("Booking not found", HttpStatus.NOT_FOUND));
        bookingRepository.deleteById(bookingId);
    }
}
