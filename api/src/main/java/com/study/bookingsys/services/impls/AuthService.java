package com.study.bookingsys.services.impls;

import com.study.bookingsys.configs.jwt.JwtUtil;
import com.study.bookingsys.enums.ERole;
import com.study.bookingsys.models.dtos.AuthRequestDTO;
import com.study.bookingsys.models.dtos.AuthResponseDTO;
import com.study.bookingsys.models.dtos.RegisterDTO;
import com.study.bookingsys.models.entities.Profile;
import com.study.bookingsys.models.entities.User;
import com.study.bookingsys.respositories.UserRepository;
import com.study.bookingsys.services.IAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Optional;


@Service
public class AuthService implements IAuthService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public AuthResponseDTO authenticate(AuthRequestDTO request) {
        User user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new RuntimeException("User not found"));
        if (!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND, "Invalid password");
        }
        return AuthResponseDTO.builder()
                .token(jwtUtil.generateToken(user))
                .build();
    }

    @Override
    public void register(RegisterDTO request) {
        Optional<User> foundUser = userRepository.findByEmail(request.getEmail());
        if (foundUser.isPresent()) {
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND, "Email already taken");
        }
        Profile profile = Profile.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .build();
        User user = User.builder()
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(ERole.ROLE_USER)
                .locked(false)
                .profile(profile)
                .build();
        userRepository.save(user);
    }
}
