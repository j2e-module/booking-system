package com.study.bookingsys.services.impls;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.entities.User;
import com.study.bookingsys.services.IAuthoritiesManagerService;
import com.study.bookingsys.utils.UserUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class AuthoritiesManagerServiceService implements IAuthoritiesManagerService {

    @Override
    public void assertOwnerAccess(HttpServletRequest request, Long ownerId) throws HttpException {
        User currentUser = UserUtils.getUserFromRequest(request);
        if (!currentUser.getId().equals(ownerId)) {
            throw new HttpException("Access denied", HttpStatus.FORBIDDEN);
        }
    }
}
