package com.study.bookingsys.enums;

public enum ESearchOperation {

    CONTAINS, EQUAL, GREATER_THAN_EQUAL, ALL, ANY, NOT_EQUAL, EQ_BOOLEAN, LESS_THAN_EQUAL, NOT_EXISTS_INSIDE_INTERVAL;

    public static ESearchOperation getDataOption(final String dataOption){
        switch(dataOption.toLowerCase()){
            case "all": return ALL;
            case "any": return ANY;
            default: return null;
        }
    }
    public static ESearchOperation getSimpleOperation(final String input) {
        switch (input){
            case "cn": return CONTAINS;
            case "eq": return EQUAL;
            case "ne": return NOT_EQUAL;
            case "ge": return GREATER_THAN_EQUAL;
            case "le": return LESS_THAN_EQUAL;
            case "eqb": return EQ_BOOLEAN;
            case "nei": return NOT_EXISTS_INSIDE_INTERVAL;
            default: return null;
        }
    }
}
