package com.study.bookingsys.enums;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
