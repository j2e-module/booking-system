package com.study.bookingsys.enums;

public enum ERoomType {
    SINGLE,
    DOUBLE,
    TWIN,
    QUEEN,
    KING,
    SUITE,
    OTHER
}
