package com.study.bookingsys.utils;

import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

public class ImageUtils {

    private final static List<String> imageTypes = Arrays.asList("image/jpeg", "image/png", "image/jpg");

    public static boolean isImageNotValid(MultipartFile image){
        if(image.isEmpty())
            return true;
        return !imageTypes.contains(image.getContentType());
    }

    public static boolean isImagesNotValid(List<MultipartFile> images){
        boolean invalid = false;
        if(images.isEmpty())
            return invalid;
        for (MultipartFile image : images) {
            if(ImageUtils.isImageNotValid(image)){
                invalid = true;
                break;
            }
        }
        return invalid;
    }
}
