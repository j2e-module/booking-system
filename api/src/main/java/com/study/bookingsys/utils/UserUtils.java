package com.study.bookingsys.utils;

import com.study.bookingsys.models.entities.User;
import jakarta.servlet.http.HttpServletRequest;

public class UserUtils {

    public static final String USER_NOT_FOUND = "User not found";
    public static final String USER_ALREADY_EXISTS = "User already exists";

    public static User getUserFromRequest(HttpServletRequest request){
        return (User) request.getAttribute("user");
    }
}
