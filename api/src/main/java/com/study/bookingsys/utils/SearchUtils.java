package com.study.bookingsys.utils;

import com.study.bookingsys.models.dtos.SearchCriteriaDTO;
import com.study.bookingsys.models.dtos.SearchDTO;
import com.study.bookingsys.specifications.SpecificationBuilder;

import java.util.List;

public class SearchUtils {

    public static SpecificationBuilder getBuilderWithSearchCriteria(SearchDTO search){
        SpecificationBuilder builder = new SpecificationBuilder();
        List<SearchCriteriaDTO> criteriaList = search.getSearchCriteriaList();
        if(criteriaList != null){
            criteriaList.forEach(x-> {
                x.setDataOption(search.getDataOption());
                builder.with(x);
            });
        }
        return builder;
    }
}
