package com.study.bookingsys.configs.exceptions;

import com.study.bookingsys.models.dtos.HttpMessageResponse;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionsHandler.class);

    @ExceptionHandler(HttpException.class)
    public ResponseEntity<HttpMessageResponse> handleHttpException(
            HttpException ex, WebRequest request) {
        return new ResponseEntity<HttpMessageResponse>(
                HttpMessageResponse.builder().message(ex.getMessage()).build(),
                ex.getStatus());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<HttpMessageResponse> handleAccessDeniedException(
            Exception ex, WebRequest request) {
        return new ResponseEntity<HttpMessageResponse>(
                HttpMessageResponse.builder().message("You are not authorized").build(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<HttpMessageResponse> handleTokenExpiredException(
            ExpiredJwtException ex, WebRequest request) {
        return new ResponseEntity<HttpMessageResponse>(
                HttpMessageResponse.builder().message("Token Expired").build(),
                HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<HttpMessageResponse> handleAll(
            Exception ex, WebRequest request) {
        LOGGER.error("Error", ex);
        return new ResponseEntity<HttpMessageResponse>(
                HttpMessageResponse.builder().message("Something went wrong").build(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ MalformedJwtException.class, SignatureException.class, JwtException.class })
    public ResponseEntity<HttpMessageResponse> handleTokenMalformedException(
            Exception ex, WebRequest request) {
        return new ResponseEntity<HttpMessageResponse>(
                HttpMessageResponse.builder().message("Bad Token").build(),
                HttpStatus.UNAUTHORIZED);
    }
}
