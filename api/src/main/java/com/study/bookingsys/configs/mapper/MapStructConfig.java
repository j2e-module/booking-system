package com.study.bookingsys.configs.mapper;

import com.study.bookingsys.mappers.BookingMapper;
import com.study.bookingsys.mappers.EquipmentMapper;
import com.study.bookingsys.mappers.ImageMapper;
import com.study.bookingsys.mappers.ProfileMapper;
import com.study.bookingsys.mappers.RoomMapper;
import com.study.bookingsys.mappers.UserMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapStructConfig {

    @Bean
    public ProfileMapper profileMapper() {
        return Mappers.getMapper(ProfileMapper.class);
    }

    @Bean
    public UserMapper userMapper() {
        return Mappers.getMapper(UserMapper.class);
    }

    @Bean
    public RoomMapper roomMapper() {
        return Mappers.getMapper(RoomMapper.class);
    }

    @Bean
    public BookingMapper bookingMapper() {
        return Mappers.getMapper(BookingMapper.class);
    }

    @Bean
    public EquipmentMapper equipmentMapper() {
        return Mappers.getMapper(EquipmentMapper.class);
    }

    @Bean
    public ImageMapper imageMapper() {
        return Mappers.getMapper(ImageMapper.class);
    }
}
