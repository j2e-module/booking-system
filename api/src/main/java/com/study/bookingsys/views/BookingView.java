package com.study.bookingsys.views;

public class BookingView {

    public static class GeneralBookingView {};

    public static class AdminBookingView extends GeneralBookingView {};
}
