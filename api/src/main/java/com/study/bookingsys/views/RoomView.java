package com.study.bookingsys.views;

public class RoomView {
    public static class GeneralRoomView {};

    public static class AdminRoomView extends GeneralRoomView {};
}
