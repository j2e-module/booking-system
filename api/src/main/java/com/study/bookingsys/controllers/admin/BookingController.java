package com.study.bookingsys.controllers.admin;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.BookingDTO;
import com.study.bookingsys.models.dtos.HttpMessageResponse;
import com.study.bookingsys.services.IBookingService;
import com.study.bookingsys.views.BookingView;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("adminBookingController")
@RequestMapping("/admin/booking")
public class BookingController {

    @Autowired
    private IBookingService bookingService;

    @GetMapping
    public ResponseEntity<MappingJacksonValue> getAllBookings(HttpServletRequest request,
                                                              @RequestParam(defaultValue = "0") int page,
                                                              @RequestParam(defaultValue = "10") int size) throws HttpException {
        MappingJacksonValue value = new MappingJacksonValue(bookingService.getAllBookings(request, page, size));
        value.setSerializationView(BookingView.AdminBookingView.class);
        return ResponseEntity.ok(value);
    }

    @PutMapping
    public ResponseEntity<MappingJacksonValue> updateBooking(HttpServletRequest request,
                                                             @RequestParam Long bookingId,
                                                             @RequestBody BookingDTO bookingDTO) throws HttpException {
        MappingJacksonValue value = new MappingJacksonValue(bookingService.updateBooking(request, bookingId, bookingDTO));
        value.setSerializationView(BookingView.AdminBookingView.class);
        return ResponseEntity.ok(value);
    }

    @DeleteMapping
    public ResponseEntity<HttpMessageResponse> deleteBooking(HttpServletRequest request,
                                                             @RequestParam Long bookingId) throws HttpException {
        bookingService.deleteBooking(request, bookingId);
        return new ResponseEntity<>(HttpMessageResponse.builder().message("Booking deleted successfully").build(), HttpStatus.OK);
    }
}
