package com.study.bookingsys.controllers.user;

import com.fasterxml.jackson.annotation.JacksonAnnotationValue;
import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.BookingDTO;
import com.study.bookingsys.models.dtos.BookingRequestDTO;
import com.study.bookingsys.models.dtos.RoomDTO;
import com.study.bookingsys.services.IBookingService;
import com.study.bookingsys.services.IRoomService;
import com.study.bookingsys.views.BookingView;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("userBookingController")
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    private IBookingService bookingService;

    @GetMapping("/personal")
    public ResponseEntity<MappingJacksonValue> getPersonalBookings(HttpServletRequest request,
                                                                   @RequestParam(defaultValue = "0") int page,
                                                                   @RequestParam(defaultValue = "10") int size) throws HttpException {
        MappingJacksonValue value = new MappingJacksonValue(bookingService.getAllPersonalBookings(request, page, size));
        value.setSerializationView(BookingView.GeneralBookingView.class);
        return ResponseEntity.ok(value);
    }

    @PostMapping("/{roomId}/book")
    public ResponseEntity<MappingJacksonValue> bookRoom(@PathVariable Long roomId, @RequestBody BookingRequestDTO bookingDTO,
                                               HttpServletRequest request) throws HttpException {
        MappingJacksonValue value = new MappingJacksonValue(bookingService.bookRoom(roomId, bookingDTO, request));
        value.setSerializationView(BookingView.GeneralBookingView.class);
        return new ResponseEntity<>(value, HttpStatus.CREATED);
    }
}
