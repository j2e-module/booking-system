package com.study.bookingsys.controllers.admin;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.HttpMessageResponse;
import com.study.bookingsys.models.dtos.RoomDTO;
import com.study.bookingsys.services.IRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController("adminRoomController")
@RequestMapping("/admin/room")
public class RoomController {

    @Autowired
    private IRoomService roomService;

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<RoomDTO> addRoom(@RequestPart RoomDTO roomDTO,
                                           @RequestParam(value = "images", required = false) List<MultipartFile> images) throws HttpException {
        return new ResponseEntity<>(roomService.addRoom(roomDTO, images), HttpStatus.CREATED);
    }

    @PutMapping(value = "{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<RoomDTO> updateRoom(@PathVariable Long id, @RequestPart RoomDTO roomDTO,
                                              @RequestParam(value = "images", required = false) List<MultipartFile> images) throws HttpException {
        return new ResponseEntity<>(roomService.updateRoom(id, roomDTO, images), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteRoom(@PathVariable Long id) throws HttpException {
        roomService.deleteRoom(id);
        return ResponseEntity.ok(HttpMessageResponse.builder().message("Room deleted successfully").build());
    }
}
