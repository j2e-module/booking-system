package com.study.bookingsys.controllers;

import com.study.bookingsys.models.dtos.AuthRequestDTO;
import com.study.bookingsys.models.dtos.AuthResponseDTO;
import com.study.bookingsys.models.dtos.HttpMessageResponse;
import com.study.bookingsys.models.dtos.RegisterDTO;
import com.study.bookingsys.services.IAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("auth")
public class AuthController {

    @Autowired
    private IAuthService authService;

    @PostMapping("/register")
    public ResponseEntity<HttpMessageResponse> register(@RequestBody RegisterDTO register) {
        authService.register(register);
        return ResponseEntity.ok(HttpMessageResponse.builder().message("User registered successfully").build());
    }

    @PostMapping("/login")
    public ResponseEntity<AuthResponseDTO> login(@RequestBody AuthRequestDTO authRequest) {
        return ResponseEntity.ok(authService.authenticate(authRequest));
    }
}
