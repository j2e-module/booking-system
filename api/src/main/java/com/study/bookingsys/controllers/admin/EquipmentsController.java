package com.study.bookingsys.controllers.admin;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.EquipmentDTO;
import com.study.bookingsys.models.dtos.HttpMessageResponse;
import com.study.bookingsys.services.IEquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/equipment")
public class EquipmentsController {

    @Autowired
    private IEquipmentService equipmentService;

    @GetMapping
    public ResponseEntity<List<EquipmentDTO>> getAllEquipments() {
        return ResponseEntity.ok(equipmentService.getAllEquipments());
    }

    @PostMapping
    public ResponseEntity<EquipmentDTO> addEquipment(@RequestBody EquipmentDTO equipmentDTO) {
        return new ResponseEntity<>(equipmentService.addEquipment(equipmentDTO), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<EquipmentDTO> updateEquipment(@PathVariable Long id, @RequestBody EquipmentDTO equipmentDTO) throws HttpException  {
        return new ResponseEntity<>(equipmentService.updateEquipment(id, equipmentDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteEquipment(@PathVariable Long id) throws HttpException {
        equipmentService.deleteEquipment(id);
        return new ResponseEntity<>(HttpMessageResponse.builder().message("Equipment deleted successfully").build(), HttpStatus.OK);
    }
}
