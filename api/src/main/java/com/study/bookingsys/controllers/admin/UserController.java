package com.study.bookingsys.controllers.admin;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.HttpMessageResponse;
import com.study.bookingsys.models.dtos.UserDTO;
import com.study.bookingsys.services.IUserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("id") Long id) throws HttpException {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<HttpMessageResponse> updateUser(@RequestBody UserDTO userDTO, @PathVariable("id") Long id) throws HttpException {
        userService.updateUser(id, userDTO);
        return ResponseEntity.ok(HttpMessageResponse.builder().message("User updated successfully").build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpMessageResponse> deleteUser(@PathVariable("id") Long id, HttpServletRequest request) throws HttpException {
        userService.deleteUser(id, request);
        return ResponseEntity.ok(HttpMessageResponse.builder().message("User deleted successfully").build());
    }
}
