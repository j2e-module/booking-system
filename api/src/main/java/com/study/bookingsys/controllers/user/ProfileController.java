package com.study.bookingsys.controllers.user;


import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.models.dtos.HttpMessageResponse;
import com.study.bookingsys.models.dtos.ImageDTO;
import com.study.bookingsys.models.dtos.ProfileDTO;
import com.study.bookingsys.models.entities.Profile;
import com.study.bookingsys.services.IProfileService;
import com.study.bookingsys.services.impls.ProfileService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController("userRoomController")
@RequestMapping("profile")
public class ProfileController {

    @Autowired
    private IProfileService profileService;

    @GetMapping("/user/{id}")
    public ResponseEntity<ProfileDTO> getUserProfile(HttpServletRequest request,
                                                     @PathVariable Long id) throws HttpException {
        return ResponseEntity.ok(profileService.getProfile(request, id));
    }

    @PutMapping
    public ResponseEntity<HttpMessageResponse> updateProfile(@RequestBody ProfileDTO profileDTO,
                                                             HttpServletRequest request) throws HttpException {
        profileService.updateProfile(profileDTO, request);
        return ResponseEntity.ok(HttpMessageResponse.builder().message("Profile updated successfully").build());
    }

    @PutMapping(value = "image", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ImageDTO> updateProfileImage(@RequestPart MultipartFile image,
                                                       HttpServletRequest request) throws HttpException {
        ;
        return new ResponseEntity<>(profileService.updateProfileImage(image, request), HttpStatus.OK);
    }
}
