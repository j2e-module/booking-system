package com.study.bookingsys.controllers.user;

import com.study.bookingsys.configs.exceptions.HttpException;
import com.study.bookingsys.enums.ERole;
import com.study.bookingsys.models.dtos.BookingDTO;
import com.study.bookingsys.models.dtos.BookingRequestDTO;
import com.study.bookingsys.models.dtos.RoomDTO;
import com.study.bookingsys.models.dtos.SearchDTO;
import com.study.bookingsys.models.entities.Room;
import com.study.bookingsys.services.IRoomService;
import com.study.bookingsys.views.RoomView;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/room")
public class RoomController {

    @Autowired
    private IRoomService roomService;

    @PostMapping("/search")
    public ResponseEntity<MappingJacksonValue> getAllRooms(@RequestBody SearchDTO searchDTO,
                                                           @RequestParam(defaultValue = "0") int page,
                                                           @RequestParam(defaultValue = "10") int size,
                                                           HttpServletRequest request) {
        MappingJacksonValue value = new MappingJacksonValue(roomService.getAllRooms(searchDTO, page, size));
        if(request.isUserInRole(ERole.ROLE_ADMIN.name()))
            value.setSerializationView(RoomView.AdminRoomView.class);
        else
            value.setSerializationView(RoomView.GeneralRoomView.class);
        return ResponseEntity.ok(value);
    }

    @GetMapping("{id}")
    public ResponseEntity<RoomDTO> getRoomById(@PathVariable Long id) throws HttpException {
        return ResponseEntity.ok(roomService.getRoom(id));
    }
}
