package com.study.bookingsys.respositories;


import com.study.bookingsys.models.entities.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentsRepository extends JpaRepository<Equipment, Long> {
}
