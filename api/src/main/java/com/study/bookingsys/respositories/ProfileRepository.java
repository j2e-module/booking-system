package com.study.bookingsys.respositories;

import com.study.bookingsys.enums.ERole;
import com.study.bookingsys.models.entities.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

    @Query("SELECT p FROM Profile p WHERE p.user.id = ?1 AND p.user.role = 'ROLE_USER'")
    Optional<Profile> findSafelyById(Long id);

    Optional<Profile> findByUserId(Long id);
}
