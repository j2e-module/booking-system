package com.study.bookingsys.respositories;

import com.study.bookingsys.models.entities.Booking;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {

    @Query("SELECT CASE WHEN COUNT(b) > 0 THEN TRUE ELSE FALSE END FROM Booking b "
            + "WHERE b.room.id = ?1 AND ((b.checkIn BETWEEN ?2 AND ?3) OR (b.checkOut BETWEEN ?2 AND ?3))")
    boolean isThereBookedRoom(Long roomId, LocalDateTime checkIn, LocalDateTime checkOut);

    @Query("SELECT CASE WHEN COUNT(b) > 0 THEN TRUE ELSE FALSE END FROM Booking b "
            + "WHERE b.room.id = ?1 AND ((b.checkIn BETWEEN ?2 AND ?3) OR (b.checkOut BETWEEN ?2 AND ?3)) AND b.id != ?4")
    boolean isThereBookedRoomExceptCurrent(Long roomId, LocalDateTime checkIn, LocalDateTime checkOut, Long currentId);

    Page<Booking> findAllByUserId(Long userId, PageRequest pageRequest);

}
