package com.study.bookingsys.mappers;

import com.study.bookingsys.configs.mapper.GlobalMapperConfig;
import com.study.bookingsys.models.dtos.UserDTO;
import com.study.bookingsys.models.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = GlobalMapperConfig.class)
public interface UserMapper {
    UserDTO entityToUserDTO(User user);
    void userDTOToEntity(UserDTO userDt, @MappingTarget User user);
    List<UserDTO> entitiesToUserDTOs(List<User> users);
    List<User> userDTOsToEntities(List<UserDTO> userDtos);
}
