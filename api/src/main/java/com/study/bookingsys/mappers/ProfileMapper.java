package com.study.bookingsys.mappers;

import com.study.bookingsys.configs.mapper.GlobalMapperConfig;
import com.study.bookingsys.models.dtos.ProfileDTO;
import com.study.bookingsys.models.entities.Profile;
import org.mapstruct.Mapper;

@Mapper(config = GlobalMapperConfig.class)
public interface ProfileMapper {
    ProfileDTO entityToProfileDTO(Profile profile);
    Profile profileDTOToEntity(ProfileDTO profileDto);
}
