package com.study.bookingsys.mappers;

import com.study.bookingsys.configs.mapper.GlobalMapperConfig;
import com.study.bookingsys.models.dtos.EquipmentDTO;
import com.study.bookingsys.models.entities.Equipment;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = GlobalMapperConfig.class)
public interface EquipmentMapper {

    EquipmentDTO entityToEquipmentDTO(Equipment equipment);

    void dtoToEquipmentEntity(EquipmentDTO equipmentDTO, @MappingTarget Equipment equipment);

    List<EquipmentDTO> equipmentsToEquipmentDTOs(List<Equipment> equipments);
}
