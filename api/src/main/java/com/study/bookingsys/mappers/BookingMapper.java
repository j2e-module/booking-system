package com.study.bookingsys.mappers;

import com.study.bookingsys.configs.mapper.GlobalMapperConfig;
import com.study.bookingsys.models.dtos.BookingDTO;
import com.study.bookingsys.models.entities.Booking;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = GlobalMapperConfig.class)
public interface BookingMapper {
    
    void bookingDTOToEntity(BookingDTO bookingDto, @MappingTarget  Booking booking);

    void entityToBookingDTO(Booking booking, @MappingTarget BookingDTO bookingDto);

    List<BookingDTO> entitiesToBookingDTOs(List<Booking> bookings);

    BookingDTO entityToBookingDTO(Booking booking);
}
