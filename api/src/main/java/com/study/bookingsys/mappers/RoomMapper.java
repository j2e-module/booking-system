package com.study.bookingsys.mappers;

import com.study.bookingsys.configs.mapper.GlobalMapperConfig;
import com.study.bookingsys.models.dtos.RoomDTO;
import com.study.bookingsys.models.entities.Room;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(config = GlobalMapperConfig.class, uses = {EquipmentMapper.class})
public interface RoomMapper {

    RoomDTO entityToRoomDTO(Room room);

    void roomDTOToEntity(RoomDTO roomDto, @MappingTarget Room room);

    List<Room> roomDTOsToEntities(List<RoomDTO> roomDtos);

    List<RoomDTO> roomEntitiesToDTOs(List<Room> rooms);
}
