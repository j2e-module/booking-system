package com.study.bookingsys.mappers;

import com.study.bookingsys.configs.mapper.GlobalMapperConfig;
import com.study.bookingsys.models.dtos.ImageDTO;
import com.study.bookingsys.models.entities.Image;
import org.mapstruct.Mapper;

@Mapper(config = GlobalMapperConfig.class)
public interface ImageMapper {

    ImageDTO entityToImageDTO(Image image);
}
