package com.study.bookingsys.specifications;

import com.study.bookingsys.enums.ERoomType;
import com.study.bookingsys.enums.ESearchOperation;
import com.study.bookingsys.models.dtos.SearchCriteriaDTO;
import com.study.bookingsys.models.entities.Booking;
import com.study.bookingsys.models.entities.Room;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


@AllArgsConstructor
public class RoomSpecification implements Specification<Room> {

    private final SearchCriteriaDTO searchCriteria;

    @Override
    public Predicate toPredicate(Root<Room> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Object strToSearch = searchCriteria.getValue().toLowerCase();
        if (searchCriteria.getFilterKey().equals("type"))
            strToSearch = ERoomType.valueOf(((String) strToSearch).toUpperCase());
        Predicate predicate = null;
        switch (Objects.requireNonNull(ESearchOperation.getSimpleOperation(searchCriteria.getOperation()))) {
            case CONTAINS:
                predicate = criteriaBuilder.like(criteriaBuilder.lower(root.get(searchCriteria.getFilterKey())), "%" + strToSearch + "%");
                break;
            case EQUAL:
                predicate = criteriaBuilder.equal(root.get(searchCriteria.getFilterKey()), strToSearch);
                break;
            case NOT_EQUAL:
                predicate = criteriaBuilder.notEqual(root.get(searchCriteria.getFilterKey()), strToSearch);
                break;
            case GREATER_THAN_EQUAL:
                predicate = criteriaBuilder.greaterThanOrEqualTo(root.get(searchCriteria.getFilterKey()), strToSearch.toString());
                break;
            case LESS_THAN_EQUAL:
                predicate = criteriaBuilder.lessThanOrEqualTo(root.get(searchCriteria.getFilterKey()), strToSearch.toString());
                break;
            case EQ_BOOLEAN:
                if(searchCriteria.getFilterKey().equals("isConfirmed")){
                    predicate = criteriaBuilder.or(
                            criteriaBuilder.isNull(roomBookingJoin(root).get("id")),
                            criteriaBuilder.equal(roomBookingJoin(root).get(searchCriteria.getFilterKey()), Boolean.parseBoolean(strToSearch.toString()))
                    );
                }
                break;
            case NOT_EXISTS_INSIDE_INTERVAL:
                if(searchCriteria.getFilterKey().equals("bookings")) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
                    LocalDateTime checkIn = LocalDateTime.parse(searchCriteria.getValue(), formatter);
                    LocalDateTime checkOut = LocalDateTime.parse(searchCriteria.getSecondValue(), formatter);

                    Predicate bookingExistsInInterval = criteriaBuilder.and(
                            criteriaBuilder.between(roomBookingJoin(root).get("checkIn"), checkIn, checkOut).not(),
                            criteriaBuilder.between(roomBookingJoin(root).get("checkOut"), checkIn, checkOut).not()
                    );

                    predicate = criteriaBuilder.or(
                            criteriaBuilder.isNull(roomBookingJoin(root).get("id")),
                            bookingExistsInInterval
                    );
                }
                break;
            default:
                break;
        }
        return predicate;
    }

    private Join<Room, Booking> roomBookingJoin(Root<Room> root) {
        return root.join("bookingList", JoinType.LEFT);
    };
}
