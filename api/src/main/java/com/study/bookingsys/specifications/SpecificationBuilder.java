package com.study.bookingsys.specifications;

import com.study.bookingsys.enums.ESearchOperation;
import com.study.bookingsys.models.dtos.SearchCriteriaDTO;
import com.study.bookingsys.models.entities.Room;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class SpecificationBuilder {

    private final List<SearchCriteriaDTO> params = new ArrayList<>();

    public void with(SearchCriteriaDTO searchCriteria){
        params.add(searchCriteria);
    }

    public Specification<Room> buildRooms() {
        if (params.isEmpty()) {
            return null;
        }
        Specification<Room> result = new RoomSpecification(params.get(0));
        for (int idx = 1; idx < params.size(); idx++) {
            SearchCriteriaDTO criteria = params.get(idx);
            if (ESearchOperation.getDataOption(criteria.getDataOption()) == ESearchOperation.ALL)
                result = Specification.where(result).and(new RoomSpecification(criteria));
            else
                result = Specification.where(result).or(new RoomSpecification(criteria));
        }
        return Specification.where(result);
    }

}

