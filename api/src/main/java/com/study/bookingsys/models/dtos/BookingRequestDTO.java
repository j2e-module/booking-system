package com.study.bookingsys.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookingRequestDTO {

    private String note;

    private LocalDateTime checkIn;

    private LocalDateTime checkOut;

}
