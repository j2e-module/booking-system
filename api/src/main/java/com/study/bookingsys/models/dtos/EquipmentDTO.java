package com.study.bookingsys.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.study.bookingsys.views.RoomView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EquipmentDTO {

    @JsonView(RoomView.GeneralRoomView.class)
    private Long id;

    @JsonView(RoomView.GeneralRoomView.class)
    private String name;

    @JsonView(RoomView.GeneralRoomView.class)
    private String description;
}
