package com.study.bookingsys.models.dtos;

import com.fasterxml.jackson.annotation.JsonView;
import com.study.bookingsys.views.RoomView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonView(RoomView.GeneralRoomView.class)
public class ImageDTO {

    private String name;

    private String type;

    private String url;
}
