package com.study.bookingsys.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProfileDTO {

    private String firstName;

    private String lastName;

    private String phone;

    private String bio;

    private ImageDTO image;

}
