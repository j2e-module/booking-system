package com.study.bookingsys.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchCriteriaDTO {
    private String filterKey;
    private String value;
    private String secondValue;
    private String operation;
    private String dataOption;

    public SearchCriteriaDTO(String filterKey, String operation, String value){
        this.filterKey = filterKey;
        this.operation = operation;
        this.value = value;
    }

    public SearchCriteriaDTO(String filterKey, String operation, String value, String secondValue){
        this.filterKey = filterKey;
        this.operation = operation;
        this.value = value;
        this.secondValue = secondValue;
    }

}
