package com.study.bookingsys.models.dtos;

import com.fasterxml.jackson.annotation.JacksonAnnotation;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.study.bookingsys.enums.ERoomType;
import com.study.bookingsys.models.entities.Image;
import com.study.bookingsys.views.BookingView;
import com.study.bookingsys.views.RoomView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomDTO {

    @JsonView({RoomView.GeneralRoomView.class, BookingView.GeneralBookingView.class})
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @JsonView(RoomView.GeneralRoomView.class)
    private String name;

    @JsonView(RoomView.GeneralRoomView.class)
    private String description;

    @JsonView(RoomView.GeneralRoomView.class)
    private Double price;

    @JsonView(RoomView.GeneralRoomView.class)
    private Integer capacity;

    @JsonView(RoomView.GeneralRoomView.class)
    private ERoomType type;

    @JsonView(RoomView.AdminRoomView.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime createdAt;

    @JsonView(RoomView.AdminRoomView.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime updatedAt;

    @JsonView(RoomView.GeneralRoomView.class)
    private List<EquipmentDTO> equipmentList;

    @JsonView(RoomView.GeneralRoomView.class)
    private List<ImageDTO> imageList;
}
