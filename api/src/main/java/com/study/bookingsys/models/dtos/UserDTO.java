package com.study.bookingsys.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.study.bookingsys.views.BookingView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    @JsonView(BookingView.AdminBookingView.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @JsonView(BookingView.AdminBookingView.class)
    private String email;

    private String role;

    private boolean locked;

    private LocalDate createdAt;

    private LocalDate updatedAt;

    private ProfileDTO profile;
}
