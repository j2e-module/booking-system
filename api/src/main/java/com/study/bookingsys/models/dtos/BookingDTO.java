package com.study.bookingsys.models.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.study.bookingsys.views.BookingView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookingDTO {

    @JsonView(BookingView.GeneralBookingView.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @JsonView(BookingView.GeneralBookingView.class)
    private String note;

    @JsonView(BookingView.GeneralBookingView.class)
    private LocalDateTime checkIn;

    @JsonView(BookingView.GeneralBookingView.class)
    private LocalDateTime checkOut;

    @JsonView(BookingView.GeneralBookingView.class)
    private boolean isConfirmed;

    @JsonView(BookingView.GeneralBookingView.class)
    private boolean isCancelled;

    @JsonView(BookingView.GeneralBookingView.class)
    private boolean isPaid;

    @JsonView(BookingView.AdminBookingView.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime createdAt;

    @JsonView(BookingView.AdminBookingView.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime updatedAt;

    @JsonView(BookingView.AdminBookingView.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UserDTO user;

    @JsonView(BookingView.GeneralBookingView.class)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private RoomDTO room;
}
